# Container Orchestration with Kubernetes
This project will cover a start to finish deployment of a Stateful Java App with MySQL to a Kubernetes cluster.
## Technologies Used
- Kubernetes
- Linode LKE
- Java
- MySQL
- PHPMyAdmin
- Helm and Helm Chart

## Project Description
- Create and Configure a Managed LKE (Linode Kuberentes Engine) Environment
- Deploy MySQL to cluster
- Deploy Java App to cluster
- Deploy PHPMyAdmin to cluster
- Deploy Ingress Controller
- Create Ingress Rules
- Post-forwarding for PHPMyAdmin
- Create a Helm Chart for Java App

## Prerequisites
- [Project Code](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/kubernetes-exercises)
- Private Docker Repository (AWS ECR, Docker Hub, Artifactory, etc)

## Guide Steps
### EXERCISE 1: Create a Kubernetes cluster
- I will use Linode, any other provider can be used or a local Minkube cluster
- Portal > Kubernetes > **Create Cluster**
	- Cluster Name: **m10p-java-project**
	- Region: **CLOSEST_TO_YOU**
	- Kubernetes Version: **1.28**
	- HA Control Plane: **No**
	- Nodes:
		- Shared CPU > **3x Linode 4GB**
	- **Create Cluster**
- Download `Kubeconfig.yaml`
-  Connect your computer to the Linode K8s cluster
	- `export KUBECONFIG=/home/USER/Downloads/m10p-java-project-kubeconfig.yaml`

### EXERCISE 2: Deploy Mysql with 2 replicas
#### Configure your Computer with Repository
- We will use Helm to deploy MySQL with 2 Replicas. We can find information on how from [ArtifactHub for Bitnami MySQL](https://artifacthub.io/packages/helm/bitnami/mysql)
- First, we will need to add the repository so Helm knows where to look for the software package
	- `helm repo add bitnami https://charts.bitnami.com/bitnami`

#### Configure a values.yaml for MySQL
- This information can be found on the ArtifactHub page linked above, click the **Values Schema** button to get a list of all necessary values for this application that we will configure.
- We want to have a primary, and secondary database configuration, as well as authentication configured.
- Create our file, or use the pre-generated file from ArtifactHub
- `vim values-mysql.yaml`
	- I filled in basic information from the downloaded template for what I want to configure. There are four sections that we will need **auth**, **primary**, **secondary**, **architecture**
- Once completed, we can install this with Helm to the K8s cluster
	- `helm install my-app bitnami/mysql -f mysql-values.yaml`
	- **Note:** Depending on where you are running this it may take some time. I'm running this in Linode which may take some time to start everything.

![MySQL Deployed Successfully](/images/m10p-mysql-deployed.png)


### EXERCISE 3: Deploy your Java Application with 2 replicas
#### Clone Java App Repo
- Download Code
	- `git clone git@gitlab.com:twn-devops-bootcamp/latest/10-kubernetes/kubernetes-exercises.git`
- Build Code
	- `gradle build`

#### Build Docker Image
**Note:** This would normally be done in a CI/CD pipeline but for this tutorial we will bypass that and build locally and manually upload to our Private Docker Hub Repo
- Create a quick Dockerfile
```yaml
# Set base image
FROM amazoncorretto:21-alpine3.19

# Port for our App
EXPOSE 8080

# Make app directory
RUN mkdir -p /usr/app

# Copy files
COPY ./build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar /usr/app

# Set Working Directory
WORKDIR /usr/app

# Set entry for how to start the application
ENTRYPOINT ["java","-jar","bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]
```
- Build Docker Image
	- `docker build -t java-app:1.0 .`
- Tag Image
	- `docker tag java-app:1.0 REPO_NAME/java-app:1.0`
- Upload to Private Artifact Registry
	- `docker login`
	- `docker push REPO_NAME/java-app:1.0`

![Successful Docker Image Upload](/images/m10p-successful-docker-image-upload.png)

#### Configure Docker Registry Access for K8s
- We will create a my-registry-key with kubectl to to allow for access to the private Docker Registry
```bash
kubectl create secret generic my-registry-key \
--from-file=.dockerconfigjson=/home/USER/.docker/config.json \
--type=kubernetes.io/dockerconfigjson
```

![Successful Secret Creation](/images/m10p-successful-secret-creation.png)

#### Create and Apply K8s Secret
- We need to link our App to the Database with a **Secret**. It will contain our previously set values from the `values-mysql.yaml` file in base64 encoding!
	- You can use the CLI to print out the Base64 encoded values using `echo -n VALUE | base64`
- **Note:** Make sure to run `echo -n` otherwise an extra line return will be included and it set them to the wrong values.
- **Note:** For simplicity we will hard-code the base64 encoded values. Don't do this and instead you can set them to an environment variable and store things in a safer manner!

```yaml
#db-secrets.yaml
apiVersion: v1
kind: Secret
metadata:
  name: db-secret
type: Opaque
data:
  db_user: BASE64_user
  db_pwd: BASE64_password
  db_name: BASE64_name
  db_root_pwd: BASE64_root_password
```
- Apply the Secret
	- `kubectl apply -f db-secret.yaml`

#### Create and Apply K8s ConfigMap
- Similar to Secret, we need to set the database server name
- **db_server** can be obtained from `kubectl` with `kubectl get svc` and we'll set it to the primary service
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: db-config
data:
  db_server: my-app-mysql-primary.default
```
- Apply the ConfigMap
	- `kubectl apply -f db-config.yaml`

#### Create and Apply K8s Java App
- Our Java App has some requirements which we took care of in Secret and ConfigMap, however we now need to configure a Deployment and Service for this application. We will create a `java-app.yaml` with everything it needs to run.
- This configuration is fairly generic, it addresses the naming, labels, the image location/name, and the four environment variables we configured in db-secret.yaml as well as exposing port 8080.
- Set the `containers.image` to your Private Docker Repo tagged image name
- Deploy our Java App
	- `kubectl apply -f java-app.yaml`

### EXERCISE 4: Deploy phpmyadmin
- Similarly to the `java-app.yaml` we will create a Deployment and Service in a single file called `phpmyadmin.yaml`
- We will use a basic template that can be obtained from this [repository](https://github.com/serainville/serverlab-kubernetes-templates) and we will modify the fields to be our values (ports, env vars, etc)
- Deploy our PHPMyAdmin
	- `kubectl apply -f phpmyadmin.yaml`


At This point, we have our MySQL database, Java App, and PHPMyAdmin all running on a Linode Managed K8s Cluster.

![All Apps Running](/images/m10p-all-apps-running.png)

### EXERCISE 5: Deploy Ingress Controller
- We will deploy a [helm chart for ingress-nginx](https://github.com/kubernetes/ingress-nginx/tree/main/charts/ingress-nginx)
	- The RELEASE_NAME is `ingress-nginx`
- Add the repo to your LKE cluster
	- `helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx`
- Update (Just like any package manager)
	- `helm repo update`
- Install Ingress
	- `helm install ingress-nginx ingress-nginx/ingress-nginx`
		- **Note:** This can take a few minutes on Linode && it can take a few extra minutes for the Load Balancer IP to be available

![Nginx Deployed & Configured](/images/m10p-nginx-controller-configured.png)

### EXERCISE 6: Create Ingress rule
- We need to create an Ingress rule for our Java App which will use a [template from here](https://kubernetes.io/docs/concepts/services-networking/ingress/)
	- name: **java-app-ingress**
	- host: **my-java-app.com**
	- service.name: **java-app-service**
	- port.number: **8080**
	- ingressClassName: **nginx**
- Apply the Configuration
	- `kubectl apply -f java-app-ingress.yaml`

![Successful Ingress Deploy](/images/m10p-successful-ingress-deploy.png)

### EXERCISE 7: Port-forward for phpmyadmin
[Configure Port Forwarding Documentation](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)
- Configure Port Forwarding 
	- `kubectl port-forward svc/phpmyadmin-service 8081:8081`

**Note:** Your CLI window will not return control while port-forward is running. Instead lets check to make sure PHPMyAdmin is accessible via the browser.

- Navigate to 127.0.0.1:8081 in your browser of choice while the command is still running. Log in with your MySQL **user** & **pass**

![PHPMyAdmin Accessible](/images/m10p-phpmyadmin-accessible.png)


![Traffic Successfully Forwarding](/images/m10p-port-forwarding.png)

### EXERCISE 8: Create Helm Chart for Java App

Finally instead of having everything separated and hardcoded, we will convert everything to a Helm Chart. 

- Create Helm Chart
	- `helm create java-app`
- Create Templates for all Deployments, Secrets, ConfigMap
- Pull default values into a `values.yaml`
- Put specific values into respective `values-override.yaml` 
- Uninstall all Previous Pods to have a clean namespace
- Perform a Dry Run with the Helm Chart
	- `helm install my-java-app java-app -f java-app/values-override.yaml --dry-run --debug`
	- Verify all values are correct from the output. Compare them to the previous k8s config files
- Perform Install
	- `helm install my-java-app java-app -f java-app/values-override.yaml`

![Helm Chart Installed](/images/m10p-successful-helm-chart-install.png)

If this Helm Chart works, you can put the `java-app` folder into it's own GIT repository and it is now ready to be used as many times as needed.